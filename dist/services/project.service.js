"use strict";
/** @format */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProjectService = void 0;
class ProjectService {
    constructor(sequelize) {
        this.client = sequelize;
        this.models = this.client.models;
    }
    getAll(limit, offset) {
        return __awaiter(this, void 0, void 0, function* () {
            const attributes = ['id', 'user_id', 'image', 'description', 'created_at'];
            const options = {
                attributes,
            };
            if (limit)
                options.limit = parseInt(limit, 10);
            if (offset)
                options.offset = parseInt(offset, 10) - 1;
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const allProject = yield this.models.Project.findAll(options);
                    resolve(allProject);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
    add(Project) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const newProject = yield this.models.Project.create(Project);
                    resolve(newProject);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
    findById(projectId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const where = {
                        id: projectId,
                    };
                    if (userId)
                        where.userId = userId;
                    const project = yield this.models.Project.findOne({
                        where,
                    });
                    resolve(project);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
    update(project_id, data, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const where = {
                        id: project_id,
                    };
                    if (user_id)
                        where.user_id = user_id;
                    const updatedProject = yield this.models.Project.update(data, { where });
                    resolve(updatedProject);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
    deleteProject(project_id, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const where = {
                        id: project_id,
                    };
                    if (user_id)
                        where.user_id = user_id;
                    const deletedProject = yield this.models.Project.destroy({ where });
                    resolve(deletedProject);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
}
exports.ProjectService = ProjectService;
//# sourceMappingURL=project.service.js.map