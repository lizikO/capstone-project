"use strict";
/** @format */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExperienceService = void 0;
// import { Experience } from '../models/experience.model';
class ExperienceService {
    constructor(sequelize) {
        this.client = sequelize;
        this.models = this.client.models;
    }
    getAll(limit, offset) {
        return __awaiter(this, void 0, void 0, function* () {
            const attributes = ['id', 'user_id', 'company_name'];
            const options = {
                attributes,
            };
            if (limit)
                options.limit = parseInt(limit, 10);
            if (offset)
                options.offset = parseInt(offset, 10) - 1;
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const allExperience = yield this.models.Experience.findAll(options);
                    resolve(allExperience);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
    add(experience) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const newExperience = yield this.models.Experience.create(experience);
                    resolve(newExperience);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
    findById(experience_id, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const where = { id: experience_id };
                    if (user_id) {
                        where.user_id = user_id;
                    }
                    const experience = yield this.models.Experience.findOne({
                        where,
                    });
                    resolve(experience);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
    update(experience_id, data, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const where = { id: experience_id };
                    if (user_id)
                        where.userId = user_id;
                    const updatedExperience = yield this.models.Experience.update(data, {
                        where,
                    });
                    resolve(updatedExperience);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
    deleteExperience(experience_id, user_id) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const where = { id: experience_id };
                    if (user_id)
                        where.user_id = user_id;
                    const deletedExperience = yield this.models.Experience.destroy({ where });
                    resolve(deletedExperience);
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
}
exports.ExperienceService = ExperienceService;
//# sourceMappingURL=experience.service.js.map