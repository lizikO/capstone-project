"use strict";
/** @format */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// export class AuthService {}
const dotenv = __importStar(require("dotenv"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
// import { User as UserModel } from '../models/user.model';
const experience_model_1 = require("../models/experience.model");
const feedback_model_1 = require("../models/feedback.model");
const project_model_1 = require("../models/project.model");
dotenv.config();
const { SECRET_TOKEN } = process.env;
class AuthService {
    constructor(sequelize) {
        this.client = sequelize;
        this.models = this.client.models;
    }
    register(user) {
        return __awaiter(this, void 0, void 0, function* () {
            const COST = 12;
            const hashedPassword = yield bcrypt_1.default.hash(user.password, COST);
            return this.models.User.create(Object.assign(Object.assign({}, user), { password: hashedPassword }));
        });
    }
    getAll(limit, offset) {
        return __awaiter(this, void 0, void 0, function* () {
            const attributes = [
                'id',
                'first_name',
                'last_name',
                'title',
                'summary',
                'email',
                'role',
                'image',
            ];
            const options = {
                attributes,
            };
            if (limit)
                options.limit = parseInt(limit, 10);
            if (offset)
                options.offset = parseInt(offset, 10) - 1;
            return this.models.User.findAll(options);
        });
    }
    findByEmail(email) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.models.users.findAll({
                where: {
                    email,
                },
            });
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.models.User.findOne({
                where: {
                    id,
                },
                attributes: [
                    'id',
                    'first_name',
                    'last_name',
                    'title',
                    'summary',
                    'email',
                    'role',
                    'password',
                    'image',
                ],
            });
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.models.User.update(data, { where: { id }, returning: true });
        });
    }
    login(email, candidatePassword) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const user = yield this.models.User.findOne({
                        where: {
                            email,
                        },
                        attributes: [
                            'first_name',
                            'last_name',
                            'title',
                            'summary',
                            'email',
                            'role',
                            'image',
                            'password',
                            'id',
                        ],
                    });
                    const passwordIsCorrect = user
                        ? yield bcrypt_1.default.compare(candidatePassword, user.password)
                        : false;
                    if (passwordIsCorrect) {
                        const token = jsonwebtoken_1.default.sign({ id: user.id, role: user.role }, SECRET_TOKEN, {
                            expiresIn: '1d',
                        });
                        // eslint-disable-next-line @typescript-eslint/no-unused-vars
                        const _a = user.dataValues, { password } = _a, rest = __rest(_a, ["password"]);
                        resolve({
                            token,
                            user: rest,
                        });
                    }
                    else {
                        reject('Invalid email and password');
                    }
                }
                catch (error) {
                    reject(error);
                }
            }));
        });
    }
    deleteUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.models.User.destroy({
                where: {
                    id,
                },
            });
        });
    }
    CV(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.models.User.findByPk(userId, {
                include: [
                    { model: experience_model_1.Experience },
                    { model: project_model_1.Project, as: 'projects' },
                    { model: feedback_model_1.Feedback, as: 'feedbacksFromUser' },
                    { model: feedback_model_1.Feedback, as: 'feedbacksToUser' },
                ],
            });
        });
    }
}
exports.default = AuthService;
//# sourceMappingURL=auth.service.js.map