"use strict";
/** @format */
Object.defineProperty(exports, "__esModule", { value: true });
exports.Experience = void 0;
const sequelize_1 = require("sequelize");
class Experience extends sequelize_1.Model {
    static defineSchema(sequelize) {
        Experience.init({
            id: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true,
            },
            userId: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                field: 'user_id',
                allowNull: false,
                references: {
                    model: 'users',
                    key: 'id',
                },
            },
            companyName: {
                type: sequelize_1.DataTypes.STRING(256),
                field: 'company_name',
                allowNull: false,
            },
            role: {
                type: sequelize_1.DataTypes.STRING(256),
                allowNull: false,
            },
            startDate: {
                type: sequelize_1.DataTypes.DATE,
                field: 'startDate',
            },
            endDate: {
                type: sequelize_1.DataTypes.DATE,
                field: 'endDate',
            },
        }, {
            tableName: 'experiences',
            underscored: true,
            sequelize,
            // createdAt: 'created_at',
            // updatedAt: 'updated_at',
            timestamps: true,
        });
    }
    static associate() { }
}
exports.Experience = Experience;
//# sourceMappingURL=experience.model.js.map