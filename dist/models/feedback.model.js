"use strict";
/** @format */
Object.defineProperty(exports, "__esModule", { value: true });
exports.Feedback = void 0;
const sequelize_1 = require("sequelize");
class Feedback extends sequelize_1.Model {
    static defineSchema(sequelize) {
        Feedback.init({
            id: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true,
            },
            fromUser: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                field: 'from_user',
                allowNull: false,
            },
            toUser: {
                type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
                field: 'to_user',
                allowNull: false,
            },
            content: {
                type: sequelize_1.DataTypes.TEXT,
                allowNull: false,
            },
            companyName: {
                type: sequelize_1.DataTypes.STRING(256),
                field: 'company_name',
                allowNull: false,
            },
        }, {
            tableName: 'feedbacks',
            underscored: true,
            sequelize,
            timestamps: true,
        });
    }
    static associate(models) {
        Feedback.belongsTo(models.user, { foreignKey: 'from_user' });
        Feedback.belongsTo(models.user, { foreignKey: 'to_user' });
    }
}
exports.Feedback = Feedback;
//# sourceMappingURL=feedback.model.js.map