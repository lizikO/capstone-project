"use strict";
/** @format */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeFeedbackRouter = void 0;
const passport_1 = __importDefault(require("passport"));
const express_1 = __importDefault(require("express"));
const express_validator_1 = require("express-validator");
const middlewares_1 = require("../loaders/middlewares");
const validations_1 = require("../middleware/validations");
const makeFeedbackRouter = (context) => {
    const router = express_1.default.Router();
    // Define routes
    const { feedbackService, cacheService } = context.services;
    router.post('/', passport_1.default.authenticate('jwt', { session: false }), validations_1.validateFeedback, (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (req.validationErrors)
                throw new Error('Validation errors');
            const feedbackToAdd = req.body;
            if (req.user.role !== 'admin') {
                feedbackToAdd.fromUser = req.user.id;
            }
            const feedback = yield feedbackService.add(feedbackToAdd);
            res.status(201);
            res.json(feedback);
        }
        catch (error) {
            if (req.validationErrors)
                return res.status(400).send('Fields are not valid');
            next(error.message);
        }
    }));
    router.get('/', passport_1.default.authenticate('jwt', { session: false }), (0, middlewares_1.roles)(['admin']), validations_1.validatePagination, (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { pageSize, page } = req.query;
            const feedbacks = yield feedbackService.getAll(pageSize, page);
            res.status(200).header('X-total-count', feedbacks.length).json(feedbacks);
        }
        catch (error) {
            next(error.message);
        }
    }));
    router.get('/:id', passport_1.default.authenticate('jwt', { session: false }), (0, express_validator_1.param)('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const userId = req.user.role === 'admin' ? null : req.user.id;
            const feedback = yield feedbackService.findById(req.params.id, userId);
            if (!feedback) {
                return res.status(404).send('Experience with the provided ID does not exist.');
            }
            res.status(200);
            res.json(feedback);
        }
        catch (error) {
            next(error.message);
        }
    }));
    router.put('/:id', passport_1.default.authenticate('jwt', { session: false }), (0, express_validator_1.param)('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const feedbackBody = Object.assign({}, req.body);
            const userId = req.user.role === 'admin' ? null : req.user.id;
            if (userId)
                feedbackBody.fromUser = req.user.id;
            const updatedFeedback = yield feedbackService.update(req.params.id, feedbackBody, userId);
            if (!updatedFeedback[0]) {
                return res.status(404).send('Feedback with the provided ID does not exist.');
            }
            const feedback = yield feedbackService.findById(req.params.id, userId);
            yield cacheService.del(`user-${userId || req.params.id}:cv`);
            res.json(feedback);
        }
        catch (error) {
            next(error.message);
        }
    }));
    router.delete('/:id', passport_1.default.authenticate('jwt', { session: false }), (0, express_validator_1.param)('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const userId = req.user.role === 'admin' ? null : req.user.id;
            const deletedFeedback = yield feedbackService.deleteFeedback(req.params.id, userId);
            if (!deletedFeedback) {
                return res.status(404).send('Not Found');
            }
            res.status(204).send('Feedback deleted');
        }
        catch (error) {
            next(error.message);
        }
    }));
    return router;
};
exports.makeFeedbackRouter = makeFeedbackRouter;
//# sourceMappingURL=feedback.js.map