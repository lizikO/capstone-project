"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateProject = exports.validateFeedback = exports.validateExperience = exports.validateLogin = exports.validatePagination = exports.validateRegister = void 0;
const express_validator_1 = require("express-validator");
exports.validateRegister = [
    (0, express_validator_1.body)('email').isEmail().withMessage('Invalid email format'),
    (0, express_validator_1.body)('firstName').isLength({ min: 3 }).withMessage('length should be more then 3'),
    (0, express_validator_1.body)('lastName').isLength({ min: 3 }).withMessage('length should be more then 3'),
    (0, express_validator_1.body)('summary').isString().isLength({ min: 3 }).withMessage('length should be more then 3'),
    (0, express_validator_1.body)('title').isString().isLength({ min: 3 }).withMessage('length should be more then 3'),
    (0, express_validator_1.body)('password').isString().isLength({ min: 3 }).withMessage('length should be more then 3'),
    (req, res, next) => {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            req.validationErrors = errors.array();
        }
        next();
    },
];
exports.validatePagination = [
    (0, express_validator_1.query)('pageSize').optional().isInt().withMessage('pageSize must be a number'),
    (0, express_validator_1.query)('page').optional().isInt().withMessage('page must be a number'),
    (req, res, next) => {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            req.validationErrors = errors.array();
        }
        next();
    },
];
exports.validateLogin = [
    (0, express_validator_1.body)('email').isEmail().withMessage('Invalid email format'),
    (0, express_validator_1.body)('password').isString().isLength({ min: 3 }).withMessage('length should be more then 3'),
    (req, res, next) => {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            req.validationErrors = errors.array();
        }
        next();
    },
];
exports.validateExperience = [
    (0, express_validator_1.body)('companyName').isString().isLength({ min: 3 }).withMessage('Length should be more than 3'),
    (req, res, next) => {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            req.validationErrors = errors.array();
        }
        next();
    },
];
exports.validateFeedback = [
    (0, express_validator_1.body)('fromUser').isInt(),
    (0, express_validator_1.body)('toUser').isInt(),
    (0, express_validator_1.body)('content').isString(),
    (0, express_validator_1.body)('companyName').isString().isLength({ min: 3 }).withMessage('Length should be more than 3'),
    (req, res, next) => {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            req.validationErrors = errors.array();
        }
        next();
    },
];
exports.validateProject = [
    (0, express_validator_1.body)('description').isString(),
    (req, res, next) => {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            req.validationErrors = errors.array();
        }
        next();
    },
];
//# sourceMappingURL=validations.js.map