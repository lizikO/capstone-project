"use strict";
/** @format */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadApp = void 0;
const express_1 = __importDefault(require("express"));
const express_request_id_1 = __importDefault(require("express-request-id"));
const middlewares_1 = require("./middlewares");
const routes_1 = require("./routes");
const context_1 = require("./context");
const models_1 = require("./models");
const sequelize_1 = require("./sequelize");
const config_1 = require("../config");
const passport_1 = require("./passport");
const logger_1 = require("../libs/logger");
const loadApp = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const app = (0, express_1.default)();
        app.use(express_1.default.json());
        app.use(express_1.default.urlencoded({ extended: true }));
        app.use((0, express_request_id_1.default)());
        app.use(logger_1.setCorrelationId);
        const sequelize = (0, sequelize_1.loadSequelize)(config_1.config);
        yield sequelize.authenticate();
        const context = yield (0, context_1.loadContext)(sequelize, config_1.config.redis);
        process.stdout.write('\nConnected to Sequelize\n');
        (0, models_1.loadModels)(sequelize);
        (0, passport_1.loadPassport)(app, context);
        (0, middlewares_1.loadMiddlewares)(app, context);
        (0, routes_1.loadRoutes)(app, context);
        app.use(logger_1.errorHandler);
        app.use((req, res) => {
            res.status(404).json({ error: 'Not Found' });
        });
        return app;
    }
    catch (error) {
        process.exit(1);
    }
});
exports.loadApp = loadApp;
//# sourceMappingURL=app.js.map