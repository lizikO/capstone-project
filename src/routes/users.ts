/** @format */

import fs from 'fs';
import passport from 'passport';
import express, { Response, Request, NextFunction } from 'express';
import path from 'path';
import { param } from 'express-validator';
import { Context, RouterFactory } from '../interfaces/general';
import { roles, upload } from '../loaders/middlewares';
import { validatePagination, validateRegister } from '../middleware/validations';

export const makeUserRouter: RouterFactory = (context: Context) => {
  const router = express.Router();

  // Define routes
  const { cacheService, authService } = context.services;

  router.post(
    '/',
    passport.authenticate('jwt', { session: false }),
    roles(['admin']),
    upload.single('image'),
    validateRegister,
    async (req: any, res: Response, next: NextFunction) => {
      try {
        if (req.validationErrors) throw new Error('Validation errors');

        if (!req.file) {
          return res.status(400).send('No image uploaded');
        }
        const user: any = await authService.register({
          ...req.body,
          image: req.file.filename,
        });
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { password, ...rest } = user.dataValues;

        res.status(201);
        res.json(rest);
      } catch (error) {
        if (req.file) fs.unlinkSync(req.file.path);

        if (req.validationErrors) return res.status(400).send('Fields are not valid');

        next(error.message);
      }
    },
  );

  router.get(
    '/',
    passport.authenticate('jwt', { session: false }),
    roles(['admin']),
    validatePagination,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { pageSize, page } = req.query;

        const users = await authService.getAll(pageSize, page);

        res.status(200).header('X-total-count', users.length).json(users);
      } catch (error) {
        next(error.message);
      }
    },
  );

  router.get(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    param('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'),
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const userId = req.user.role === 'admin' ? req.params.id : req.user.id;

        const user = await authService.findById(userId);

        if (!user) {
          return res.status(404).send('The user with the provided ID does not exist.');
        }
        res.status(200);
        res.json(user);
      } catch (error) {
        next(error.message);
      }
    },
  );

  router.get(
    '/:id/cv',
    passport.authenticate('jwt', { session: false }),
    param('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'),
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const id = req.user.role === 'admin' ? req.params.id : req.user.id;

        const cachedCV = await cacheService.get(`user-${id}:cv`);

        if (cachedCV) return res.json(JSON.parse(cachedCV));

        const cv = await authService.CV(id);

        if (!cv) {
          return res.status(404).send('The user with the provided ID does not exist.');
        }

        await cacheService.set(`user-${id}:cv`, JSON.stringify(cv), 60000);

        res.status(200);
        res.json(cv);
      } catch (error) {
        next(error.message);
      }
    },
  );

  router.put(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    param('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'),
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const userId = req.user.role === 'admin' ? req.params.id : req.user.id;
        let userBody = { ...req.body };

        if (req.user.role !== 'admin') {
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const { image, password, role, ...rest } = req.body;
          userBody = rest;
        }

        await authService.update(userId, userBody);

        const user = await authService.findById(userId);

        if (!user) {
          return res.status(404).send('The user with the provided ID does not exist.');
        }

        res.status(200);
        res.json(user);
      } catch (error) {
        next(error.message);
      }
    },
  );

  router.delete(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    param('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'),
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const userId = req.user.role === 'admin' ? req.params.id : req.user.id;

        const { image } = await authService.findById(userId);
        const filePath = path.resolve(__dirname, `../../public/${image}`);

        const deletedUser = await authService.deleteUser(userId);

        if (!deletedUser) {
          return res.status(404).send('The user with the provided ID does not exist.');
        }
        if (fs.existsSync(filePath)) {
          fs.unlinkSync(filePath);
        }

        res.status(202).send('Deleted user account.');
      } catch (error) {
        next(error.message);
      }
    },
  );

  return router;
};
