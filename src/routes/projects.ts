/** @format */

import express, { Response, Request, NextFunction } from 'express';
import { param } from 'express-validator';
import passport from 'passport';
import fs from 'fs';
import path from 'path';
import { Context, RouterFactory } from '../interfaces/general';
import { roles, upload } from '../loaders/middlewares';
import { validatePagination, validateProject } from '../middleware/validations';

export const makeProjectsRouter: RouterFactory = (context: Context) => {
  const router = express.Router();

  const { projectService, cacheService } = context.services;

  // Define routes

  router.post(
    '/',
    passport.authenticate('jwt', { session: false }),
    upload.single('image'),
    validateProject,
    async (req: any, res: Response, next: NextFunction) => {
      try {
        if (req.validationErrors) throw new Error('Validation errors');
        if (!req.file) {
          return res.status(400).send('Invalid file. Only JPEG, PNG, and GIF images are allowed.');
        }

        const projectToAdd = { ...req.body, image: req.file.filename };

        if (req.user.role !== 'admin') {
          projectToAdd.userId = req.user.id;
        }

        const project = await projectService.add(projectToAdd);

        res.status(201);
        res.json(project);
      } catch (error) {
        if (req.file) fs.unlinkSync(req.file.path);

        if (req.validationErrors) return res.status(400).send('Fields are not valid');
        next(error.message);
      }
    },
  );

  router.get(
    '/',
    passport.authenticate('jwt', { session: false }),
    roles(['admin']),
    validatePagination,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { pageSize, page } = req.query;

        const projects: any = await projectService.getAll(pageSize, page);

        res.status(200).header('X-total-count', projects.length).json(projects);
      } catch (error) {
        next(error.message);
      }
    },
  );

  router.get(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    param('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'),
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const userId = req.user.role === 'admin' ? null : req.user.id;
        const project = await projectService.findById(req.params.id, userId);

        if (!project) {
          return res.status(404).send('The Project with the provided ID does not exist.');
        }

        res.status(200);
        res.json(project);
      } catch (error) {
        next(error.message);
      }
    },
  );

  router.put(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    param('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'),
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const userId = req.user.role === 'admin' ? null : req.user.id;

        const body = { ...req.body };

        if (userId) body.userId = userId;

        const updatedProject = await projectService.update(req.params.id, body, userId);

        if (!updatedProject[0]) {
          return res.status(404).send('Not Found');
        }

        const project = await projectService.findById(req.params.id, userId);

        await cacheService.del(`user-${userId || req.params.id}:cv`);

        res.status(200);
        res.json(project);
      } catch (error) {
        next(error.message);
      }
    },
  );

  router.delete(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    param('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'),
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const userId = req.user.role === 'admin' ? null : req.user.id;

        const project: any = await projectService.findById(req.params.id, userId);

        const filePath = path.resolve(__dirname, `../../public/${project.image}`);

        const deletedProject = await projectService.deleteProject(req.params.id, userId);

        if (!deletedProject) {
          return res.status(404).send('Not Found');
        }
        if (fs.existsSync(filePath)) {
          fs.unlinkSync(filePath);
        }
        res.status(204).send('Deleted a project');
      } catch (error) {
        next(error.message);
      }
    },
  );

  return router;
};
