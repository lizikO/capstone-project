/** @format */

import express, { NextFunction, Response } from 'express';
import fs from 'fs';
import { upload } from '../loaders/middlewares';
import { Context, RouterFactory } from '../interfaces/general';
import { validateLogin, validateRegister } from '../middleware/validations';

export const makeAuthRouter: RouterFactory = (context: Context) => {
  const router = express.Router();

  const { authService } = context.services;

  router.post(
    '/register',
    upload.single('image'),
    validateRegister,
    async (req: any, res: Response, next: NextFunction) => {
      try {
        if (req.validationErrors) throw new Error('Validation errors');
        if (!req.file) {
          return res.status(400).send('Invalid file. Only JPEG, PNG, and GIF images are allowed.');
        }

        const user: any = await authService.register({
          ...req.body,
          image: req.file.filename,
          role: 'user',
        });
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { password, ...rest } = user.dataValues;

        res.status(201);
        res.json(rest);
      } catch (error) {
        if (req.file) fs.unlinkSync(req.file.path);

        if (req.validationErrors) return res.status(400).send('Fields are not valid');
        next(error.message);
      }
    },
  );

  router.post('/login', validateLogin, async (req: any, res: Response, next: NextFunction) => {
    try {
      if (req.validationErrors) throw new Error('Validation errors');
      const userNToken = await authService.login(req.body.email, req.body.password);

      res.status(200);
      res.json(userNToken);
    } catch (error) {
      if (req.validationErrors) return res.status(400).send('Fields are not valid');

      next(error);
    }
  });

  return router;
};
