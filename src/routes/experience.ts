/** @format */

import passport from 'passport';
import express, { NextFunction, Response } from 'express';
import { param } from 'express-validator';
import { Context, RouterFactory } from '../interfaces/general';
import { roles } from '../loaders/middlewares';
import { validateExperience, validatePagination } from '../middleware/validations';

export const makeExperienceRouter: RouterFactory = (context: Context) => {
  const router = express.Router();
  const { experienceService, cacheService } = context.services;

  router.post(
    '/',
    passport.authenticate('jwt', { session: false }),
    validateExperience,
    async (req: any, res: Response, next: NextFunction) => {
      try {
        if (req.validationErrors) throw new Error('Validation errors');

        let experience;

        if (req.user.role === 'admin') {
          experience = await experienceService.add(req.body);
        } else {
          experience = await experienceService.add({
            ...req.body,
            user_id: req.user.id,
          });
        }

        res.status(201);
        res.json(experience);
      } catch (error) {
        if (req.validationErrors) return res.status(400).send('Fields are not valid');

        next(error.message);
      }
    },
  );

  router.get(
    '/',
    passport.authenticate('jwt', { session: false }),
    roles(['admin']),
    validatePagination,
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const { pageSize, page } = req.query;

        const experiences: any = await experienceService.getAll(pageSize, page);

        res.status(200).header('X-total-count', experiences.length).json(experiences);
      } catch (error) {
        next(error.message);
      }
    },
  );

  router.get(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    param('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'),
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const userId = req.user.role === 'admin' ? null : req.user.id;

        const experience = await experienceService.findById(req.params.id, userId);

        if (!experience) {
          return res.status(404).send('Experience with the provided ID does not exist.');
        }

        res.json(experience);
      } catch (error) {
        next(error.message);
      }
    },
  );

  router.put(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    param('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'),
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const userId = req.user.role === 'admin' ? null : req.user.id;
        const body = { ...req.body };

        if (userId) {
          body.userId = userId;
        }

        const updatedExperience = await experienceService.update(req.params.id, body, userId);

        if (!updatedExperience[0]) {
          return res.status(404).send('Experience with the provided ID does not exist.');
        }

        await cacheService.del(`user-${userId || req.params.id}:cv`);

        const experience = await experienceService.findById(req.params.id);
        res.json(experience);
      } catch (error) {
        next(error.message);
      }
    },
  );

  router.delete(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    param('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'),
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const userId = req.user.role === 'admin' ? null : req.user.id;

        const deletedExperience = await experienceService.deleteExperience(req.params.id, userId);

        if (!deletedExperience) {
          return res.status(404).send('Experience not found.');
        }

        res.status(204).send('Deleted experience.');
      } catch (error) {
        next(error.message);
      }
    },
  );

  return router;
};
