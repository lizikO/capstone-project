/** @format */

import passport from 'passport';
import express, { NextFunction, Response } from 'express';
import { param } from 'express-validator';
import { Context, RouterFactory } from '../interfaces/general';
import { roles } from '../loaders/middlewares';
import { validateFeedback, validatePagination } from '../middleware/validations';

export const makeFeedbackRouter: RouterFactory = (context: Context) => {
  const router = express.Router();

  // Define routes
  const { feedbackService, cacheService } = context.services;

  router.post(
    '/',
    passport.authenticate('jwt', { session: false }),
    validateFeedback,
    async (req: any, res: Response, next: NextFunction) => {
      try {
        if (req.validationErrors) throw new Error('Validation errors');

        const feedbackToAdd = req.body;

        if (req.user.role !== 'admin') {
          feedbackToAdd.fromUser = req.user.id;
        }

        const feedback = await feedbackService.add(feedbackToAdd);

        res.status(201);
        res.json(feedback);
      } catch (error) {
        if (req.validationErrors) return res.status(400).send('Fields are not valid');

        next(error.message);
      }
    },
  );

  router.get(
    '/',
    passport.authenticate('jwt', { session: false }),
    roles(['admin']),
    validatePagination,
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const { pageSize, page } = req.query;

        const feedbacks: any = await feedbackService.getAll(pageSize, page);

        res.status(200).header('X-total-count', feedbacks.length).json(feedbacks);
      } catch (error) {
        next(error.message);
      }
    },
  );

  router.get(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    param('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'),
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const userId = req.user.role === 'admin' ? null : req.user.id;

        const feedback = await feedbackService.findById(req.params.id, userId);

        if (!feedback) {
          return res.status(404).send('Experience with the provided ID does not exist.');
        }

        res.status(200);
        res.json(feedback);
      } catch (error) {
        next(error.message);
      }
    },
  );

  router.put(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    param('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'),
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const feedbackBody = { ...req.body };
        const userId = req.user.role === 'admin' ? null : req.user.id;

        if (userId) feedbackBody.fromUser = req.user.id;

        const updatedFeedback = await feedbackService.update(req.params.id, feedbackBody, userId);

        if (!updatedFeedback[0]) {
          return res.status(404).send('Feedback with the provided ID does not exist.');
        }

        const feedback = await feedbackService.findById(req.params.id, userId);

        await cacheService.del(`user-${userId || req.params.id}:cv`);

        res.json(feedback);
      } catch (error) {
        next(error.message);
      }
    },
  );

  router.delete(
    '/:id',
    passport.authenticate('jwt', { session: false }),
    param('id').isInt({ min: 1 }).withMessage('ID must be a positive integer'),
    async (req: any, res: Response, next: NextFunction) => {
      try {
        const userId = req.user.role === 'admin' ? null : req.user.id;

        const deletedFeedback = await feedbackService.deleteFeedback(req.params.id, userId);

        if (!deletedFeedback) {
          return res.status(404).send('Not Found');
        }

        res.status(204).send('Feedback deleted');
      } catch (error) {
        next(error.message);
      }
    },
  );

  return router;
};
