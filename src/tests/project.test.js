/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-undef */
const supertest = require('supertest');
const path = require('path');
const { loadApp } = require('../loaders/app');

describe('API Tests', () => {
  let app;
  let bearerToken;
  const email = 'zimbabue@gmail.com';
  const password = 'zimbabue';
  let userId;
  let projectId;

  beforeAll(async () => {
    app = await loadApp();
  });

  it('should return a 200 OK response and log in an admin user with valid email and password', async () => {
    const response = await supertest(app).post('/api/auth/login').send({ email, password });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('token', expect.any(String));

    expect(response.body).toHaveProperty('user');
    const { user } = response.body;
    userId = user.id;

    bearerToken = `Bearer ${response.body.token}`;

    expect(bearerToken).toBeDefined();

    expect(user).toHaveProperty('id', expect.any(Number));
    expect(user).toHaveProperty('email', email);
    expect(user).toHaveProperty('role', 'admin');
  });

  it('should return all project', async () => {
    const response = await supertest(app).get('/api/project').set('Authorization', bearerToken);

    expect(response.status).toBe(200);
  });

  it('should create new project', async () => {
    const filePath = `${path.resolve(__dirname, '../../public/default.png')}`;

    const response = await supertest(app)
      .post('/api/project')
      .set('Authorization', bearerToken)
      .field('userId', 1)
      .field('description', 'some new description')
      .attach('image', filePath);

    const project = response.body;

    projectId = project.id;

    expect(project).toHaveProperty('id', expect.any(Number));
    expect(project).toHaveProperty('description', expect.any(String));

    expect(response.status).toBe(201);
  });

  it('should update an project', async () => {
    const newDescription = 'project is updated';

    const response = await supertest(app)
      .put(`/api/project/${projectId}`)
      .set('Authorization', bearerToken)
      .send({
        description: newDescription,
      });

    const project = response.body;

    expect(project).toHaveProperty('description', newDescription);
    expect(project).toHaveProperty('userId', 1);
    expect(response.status).toBe(200);
  });

  it('should delete project', async () => {
    const response = await supertest(app)
      .delete(`/api/project/${projectId}`)
      .set('Authorization', bearerToken);

    expect(response.status).toBe(204);
  });
});
