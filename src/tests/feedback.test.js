/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-undef */
const supertest = require('supertest');
const { loadApp } = require('../loaders/app');

describe('API Tests', () => {
  let app;
  let bearerToken;
  const email = 'zimbabue@gmail.com';
  const password = 'zimbabue';
  let userId;
  let feedbackId;

  beforeAll(async () => {
    app = await loadApp();
  });

  it('should return a 200 OK response and log in an admin user with valid email and password', async () => {
    const response = await supertest(app).post('/api/auth/login').send({ email, password });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('token', expect.any(String));

    expect(response.body).toHaveProperty('user');
    const { user } = response.body;
    userId = user.id;

    bearerToken = `Bearer ${response.body.token}`;

    expect(bearerToken).toBeDefined();

    expect(user).toHaveProperty('id', expect.any(Number));
    expect(user).toHaveProperty('email', email);
    expect(user).toHaveProperty('role', 'admin');
  });

  it('should return all feedback', async () => {
    const response = await supertest(app).get('/api/feedback').set('Authorization', bearerToken);

    expect(response.status).toBe(200);
  });

  it('should create new feedback', async () => {
    const response = await supertest(app)
      .post('/api/feedback')
      .set('Authorization', bearerToken)
      .send({
        userId: 1,
        fromUser: 1,
        toUser: 2,
        content: 'some content',
        companyName: 'Example company name',
      });

    const feedback = response.body;

    feedbackId = feedback.id;

    expect(feedback).toHaveProperty('id', expect.any(Number));
    expect(feedback).toHaveProperty('fromUser', 1);
    expect(feedback).toHaveProperty('toUser', 2);

    expect(response.status).toBe(201);
  });

  it('should update a feedback', async () => {
    const newContent = 'feedback is updated';

    const response = await supertest(app)
      .put(`/api/feedback/${feedbackId}`)
      .set('Authorization', bearerToken)
      .send({
        content: newContent,
      });

    const feedback = response.body;

    expect(feedback).toHaveProperty('content', newContent);
    expect(feedback).toHaveProperty('fromUser', 1);
    expect(response.status).toBe(200);
  });

  it('should delete feedback', async () => {
    const response = await supertest(app)
      .delete(`/api/feedback/${feedbackId}`)
      .set('Authorization', bearerToken);

    expect(response.status).toBe(204);
  });
});
