/** @format */

import express from 'express';
import { Context } from '../interfaces/general';
import { makeAuthRouter } from '../routes/auth';
import { makeUserRouter } from '../routes/users';
import { makeExperienceRouter } from '../routes/experience';
import { makeFeedbackRouter } from '../routes/feedback';
import { makeProjectsRouter } from '../routes/projects';

export const loadRoutes = (app: express.Router, context: Context) => {
  app.use('/api/auth', makeAuthRouter(context));
  app.use('/api/user', makeUserRouter(context));
  app.use('/api/experience', makeExperienceRouter(context));
  app.use('/api/feedback', makeFeedbackRouter(context));
  app.use('/api/project', makeProjectsRouter(context));
};
