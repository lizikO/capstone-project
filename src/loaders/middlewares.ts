/** @format */

import bodyParser from 'body-parser';
import { NextFunction, Request, Response } from 'express';
import { body, validationResult } from 'express-validator';
import multer from 'multer';
import * as path from 'path';
import { Loader } from '../interfaces/general';

export const roles = (allowedRoles: any[]) => (req: any, res: Response, next: NextFunction) => {
  if (allowedRoles.includes(req.user.role)) {
    next();
  } else {
    res.status(403).send('Forbidden');
  }
};

export const validateEmail = (req: Request) =>
  new Promise<void>((resolve, reject) => {
    body('email').isEmail().withMessage('Invalid email address')(req, null, () => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        reject(errors.array());
      } else {
        resolve();
      }
    });
  });

export const loadMiddlewares: Loader = (app) => {
  app.use(bodyParser.json());
};

const publicPath = path.resolve(__dirname, '../../public');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, publicPath);
  },
  filename: (req, file, cb) => {
    const timestamp = Date.now();
    const filename = `${timestamp}-${file.originalname}`;
    cb(null, filename);
  },
});

const imageFileFilter = (req: any, file: any, cb: any) => {
  const allowedMimeTypes = ['image/jpeg', 'image/png', 'image/gif'];
  if (allowedMimeTypes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

export const upload = multer({ storage, fileFilter: imageFileFilter });
