import bcrypt from 'bcrypt';
import { Sequelize } from 'sequelize';
import { MigrationFn } from 'umzug';

export const up: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();
  const hashedPassword = await bcrypt.hash('zimbabue', 10);

  await q.bulkInsert('users', [
    {
      first_name: 'admin',
      last_name: 'admin',
      image: 'adminImage',
      title: 'some title',
      summary: 'admin summary',
      role: 'admin',
      email: 'zimbabue@gmail.com',
      password: hashedPassword,
    },
  ]);
};
