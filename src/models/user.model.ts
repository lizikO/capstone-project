/** @format */

import { DataTypes, Model, Optional, Sequelize } from 'sequelize';
import { Models } from '../interfaces/general';
import { Experience } from './experience.model';

// eslint-disable-next-line no-shadow
export enum UserRole {
  Admin = 'admin',
  User = 'user',
}

interface UserAttributes {
  id: number;
  firstName: string;
  lastName: string;
  image: string;
  title: string;
  summary: string;
  role: UserRole;
  email: string;
  password: string;
}

export class User
  extends Model<UserAttributes, Optional<UserAttributes, 'id'>>
  implements UserAttributes
{
  id: number;

  firstName: string;

  lastName: string;

  image: string;

  title: string;

  summary: string;

  role: UserRole;

  email: string;

  password: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize) {
    User.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        firstName: {
          field: 'first_name',
          type: new DataTypes.STRING(128),
          allowNull: false,
          validate: {
            len: [3, 30],
          },
        },
        lastName: {
          field: 'last_name',
          type: new DataTypes.STRING(128),
          allowNull: false,
          validate: {
            len: [3, 30],
          },
        },
        image: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        title: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        summary: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        role: {
          type: new DataTypes.STRING(50),
          allowNull: false,
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true,
          validate: {
            isEmail: true,
          },
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },
      {
        tableName: 'users',
        underscored: true,
        sequelize,
      },
    );
  }

  static associate(models: Models) {
    User.hasMany(models.experience);
    Experience.belongsTo(models.user, {
      foreignKey: {
        allowNull: false,
      },
      onDelete: 'CASCADE',
    });
    User.hasMany(models.project, {
      foreignKey: 'user_id',
      as: 'projects',
      onDelete: 'CASCADE',
    });

    User.hasMany(models.feedback, {
      foreignKey: 'from_user',
      as: 'feedbacksFromUser',
      onDelete: 'CASCADE',
    });
    User.hasMany(models.feedback, {
      foreignKey: 'to_user',
      as: 'feedbacksToUser',
      onDelete: 'CASCADE',
    });
  }
}
