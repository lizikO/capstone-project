/** @format */

import { DataTypes, Model, Optional, Sequelize } from 'sequelize';
import { Models } from '../interfaces/general';

interface FeedbackAttributes {
  id: number;
  fromUser: number;
  toUser: number;
  content: string;
  companyName: string;
}

export class Feedback
  extends Model<FeedbackAttributes, Optional<FeedbackAttributes, 'id'>>
  implements FeedbackAttributes
{
  id: number;

  fromUser: number;

  toUser: number;

  content: string;

  companyName: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize) {
    Feedback.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        fromUser: {
          type: DataTypes.INTEGER.UNSIGNED,
          field: 'from_user',
          allowNull: false,
        },
        toUser: {
          type: DataTypes.INTEGER.UNSIGNED,
          field: 'to_user',
          allowNull: false,
        },
        content: {
          type: DataTypes.TEXT,
          allowNull: false,
        },
        companyName: {
          type: DataTypes.STRING(256),
          field: 'company_name',
          allowNull: false,
        },
      },
      {
        tableName: 'feedbacks',
        underscored: true,
        sequelize,
        timestamps: true,
      },
    );
  }

  static associate(models: Models) {
    Feedback.belongsTo(models.user, { foreignKey: 'from_user' });
    Feedback.belongsTo(models.user, { foreignKey: 'to_user' });
  }
}
