/** @format */

// export class AuthService {}

import * as dotenv from 'dotenv';
import { Sequelize } from 'sequelize';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
// import { User as UserModel } from '../models/user.model';
import { Experience } from '../models/experience.model';
import { Feedback } from '../models/feedback.model';
import { Project } from '../models/project.model';

dotenv.config();

const { SECRET_TOKEN } = process.env;

export default class AuthService {
  private client: Sequelize;

  private models;

  constructor(sequelize: Sequelize) {
    this.client = sequelize;
    this.models = this.client.models;
  }

  async register(user: any) {
    const COST = 12;
    const hashedPassword = await bcrypt.hash(user.password, COST);
    return this.models.User.create({ ...user, password: hashedPassword });
  }

  async getAll(limit, offset) {
    const attributes = [
      'id',
      'first_name',
      'last_name',
      'title',
      'summary',
      'email',
      'role',
      'image',
    ];

    const options: any = {
      attributes,
    };

    if (limit) options.limit = parseInt(limit, 10);

    if (offset) options.offset = parseInt(offset, 10) - 1;

    return this.models.User.findAll(options);
  }

  async findByEmail(email: string) {
    return this.models.users.findAll({
      where: {
        email,
      },
    });
  }

  async findById(id: number) {
    return this.models.User.findOne({
      where: {
        id,
      },
      attributes: [
        'id',
        'first_name',
        'last_name',
        'title',
        'summary',
        'email',
        'role',
        'password',
        'image',
      ],
    });
  }

  async update(id: number, data: any) {
    return this.models.User.update(data, { where: { id }, returning: true });
  }

  async login(email: string, candidatePassword: string): Promise<string | false | object> {
    return new Promise(async (resolve, reject) => {
      try {
        const user: any = await this.models.User.findOne({
          where: {
            email,
          },
          attributes: [
            'first_name',
            'last_name',
            'title',
            'summary',
            'email',
            'role',
            'image',
            'password',
            'id',
          ],
        });

        const passwordIsCorrect = user
          ? await bcrypt.compare(candidatePassword, user.password)
          : false;

        if (passwordIsCorrect) {
          const token = jwt.sign({ id: user.id, role: user.role }, SECRET_TOKEN, {
            expiresIn: '1d',
          });

          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const { password, ...rest } = user.dataValues;

          resolve({
            token,
            user: rest,
          });
        } else {
          reject('Invalid email and password');
        }
      } catch (error) {
        reject(error);
      }
    });
  }

  async deleteUser(id: any) {
    return this.models.User.destroy({
      where: {
        id,
      },
    });
  }

  async CV(userId: number) {
    return this.models.User.findByPk(userId, {
      include: [
        { model: Experience },
        { model: Project, as: 'projects' },
        { model: Feedback, as: 'feedbacksFromUser' },
        { model: Feedback, as: 'feedbacksToUser' },
      ],
    });
  }
}
