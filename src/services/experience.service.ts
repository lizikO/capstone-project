/** @format */

import { Sequelize } from 'sequelize';
// import { Experience } from '../models/experience.model';

export class ExperienceService {
  private client: Sequelize;

  private models;

  constructor(sequelize: Sequelize) {
    this.client = sequelize;
    this.models = this.client.models;
  }

  async getAll(limit, offset) {
    const attributes = ['id', 'user_id', 'company_name'];

    const options: any = {
      attributes,
    };

    if (limit) options.limit = parseInt(limit, 10);

    if (offset) options.offset = parseInt(offset, 10) - 1;

    return new Promise(async (resolve, reject) => {
      try {
        const allExperience = await this.models.Experience.findAll(options);
        resolve(allExperience);
      } catch (error) {
        reject(error);
      }
    });
  }

  async add(experience: any) {
    return new Promise(async (resolve, reject) => {
      try {
        const newExperience = await this.models.Experience.create(experience);
        resolve(newExperience);
      } catch (error) {
        reject(error);
      }
    });
  }

  async findById(experience_id: number, user_id?: number) {
    return new Promise(async (resolve, reject) => {
      try {
        const where: any = { id: experience_id };
        if (user_id) {
          where.user_id = user_id;
        }

        const experience = await this.models.Experience.findOne({
          where,
        });
        resolve(experience);
      } catch (error) {
        reject(error);
      }
    });
  }

  async update(experience_id: number | string, data: any, user_id?: number) {
    return new Promise(async (resolve, reject) => {
      try {
        const where: any = { id: experience_id };
        if (user_id) where.userId = user_id;

        const updatedExperience = await this.models.Experience.update(data, {
          where,
        });

        resolve(updatedExperience);
      } catch (error) {
        reject(error);
      }
    });
  }

  async deleteExperience(experience_id: number, user_id?: number) {
    return new Promise(async (resolve, reject) => {
      try {
        const where: any = { id: experience_id };
        if (user_id) where.user_id = user_id;

        const deletedExperience = await this.models.Experience.destroy({ where });
        resolve(deletedExperience);
      } catch (error) {
        reject(error);
      }
    });
  }
}
